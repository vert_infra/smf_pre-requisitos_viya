# [VERT] Pre-requesitos SAS Viya - Ansible

## Instalar o Python PIP e os pacotes relacionados:

```bash=
 yum install -y python python-setuptools python-devel openssl-devel
 yum install -y python-pip gcc wget automake libffi-devel python-six
```
## Atualize o PIP e as ferramentas de configuração usando um dos métodos a seguir, com base na versão do Python que você está executando
```bash=
 pip install pip==19.3.1
 pip install setuptools==42.0.2
```

## Instale uma versão específica do Ansible por meio do PIP:
```bash=
pip3 install ansible==2.10.7
```
## Instale coleção ansible
```bash=
ansible-config dump -v -\-only-changed

ansible-galaxy collection install ansible.posix
ansible-galaxy collection install community.general
```


## Descompactar pacote vert_suporte
```bash=
tar -xvc vert_suporte.tar.gz
ls -ltr vert_suporte
total 0
drwxr-xr-x. 2 root root  21 Feb 24 19:59 hosts
drwxr-xr-x. 2 root root 118 Feb 24 20:02 playbook
cd vert_suporte/
```

## Preencher inventario
```bash=
vim hosts/sas.ini
[viya]
sofia001.infra.rio.gov.br
sofia002.infra.rio.gov.br
sofia003.infra.rio.gov.br
sofia004.infra.rio.gov.br
sofia005.infra.rio.gov.br
sofia006.infra.rio.gov.br
sofia007.infra.rio.gov.br
sofia008.infra.rio.gov.br
sofia009.infra.rio.gov.br
sofia010.infra.rio.gov.br

```

## Estabelecer relação de confiança com todos os hosts
```bash=
[root@localhost]# ssh-keygen 
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): (ENTER)
Created directory '/root/.ssh'.
Enter passphrase (empty for no passphrase): (ENTER)
Enter same passphrase again: 
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:c+d2lJItT3Nt15NKTejBSjarrW8cG/b1s8IFb78z7Hw root@default.lab
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|            . .  |
|           + + . |
|          o =++.+|
|        S .o=oO+*|
|         oo*.*.Oo|
|         .o.O.B o|
|          .= =.=E|
|         .o.  o=B|
+----[SHA256]-----+

```
## Copie chave para os Hosts informados em ../hosts/sas.ini
```bash=
[root@localhost ~]# ssh-copy-id root@sofia001.infra.rio.gov.b
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/root/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
root@sofia001.infra.rio.gov.b's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'root@sofia001.infra.rio.gov.b'"
and check to make sure that only the key(s) you wanted were added.

```
## Executar playbook
```
ansible-playbook  playbook/pre-requisito.yml -i hosts/sas.ini
```

<p align="center"><img src="playbook-pre-requisitos.gif?raw=true"/></p>



#### Tarefas playbook
- Consfigurar parametros em /etc/sysctl.conf
- Alterar /etc/hosts conforme inventario
- Alterar de keepcache = 0 para keepcache = 1 em /etc/yum.conf
- Alterar de #MaxStartups 0 para MaxStartups 100 em /etc/ssh/sshd_config
- Adicinar usuário SAS e CAS ao grupo wheel
- Alterar sudors %wheel ALL=(ALL) NOPASSWD: ALL
- Isntalar pacotes importantes 
- Desabilitar SELINUX
- Atualizar Sistema Operacional
- Reiniciar maquinas
- 

```

## Gerar senha criptografada para usuários criados via playbook
```bash=
pip install passlib
```
### Generate encrypted password with the command:

```bash=
python -c 'import crypt,getpass;pw=getpass.getpass();print(crypt.crypt(pw) if (pw==getpass.getpass("Confirm: ")) else exit())'
```

### Same output as before:

```bash=
Password: 
Confirm: 
$6$4QSwvTfs5ijeRo6V$qAgug/HU1WUe7e/s5c6H0HQDCb4QnOumJ6bgxyykiKgewNTr/ifF5yUBq7taNZ0eJAqrXXXwzvxd9ewgq9XHI0
```
